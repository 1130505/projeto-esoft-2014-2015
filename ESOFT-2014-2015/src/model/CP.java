package model;

/**
 *
 * @author Nuno Silva
 */

public class CP
{
    Revisor r;
    public CP()
    {
        
    }

    public Revisor addMembroCP( String strId, Utilizador u )
    {
        Revisor r = new Revisor(u);
        
        if( r.valida() && validaMembroCP(r) )
            return r;
        else
            return null;
    }
    
    private boolean validaMembroCP(Revisor r) //mesma coisa que "validaRevisor"
    {
        return true;
    }
    
    public boolean registaMembroCP(Revisor r) //mesma coisa que "guardaRevisor"
    {
        System.out.println("CP: registaMembroCP:" + r.toString());
        return true;
    }
    
}