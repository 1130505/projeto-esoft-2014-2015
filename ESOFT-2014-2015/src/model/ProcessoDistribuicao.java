/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.List;

/**
 *
 * @author Domingos
 */
public interface ProcessoDistribuicao {
    
   public List<Distribuicao> geraDistribuicoes(ProcessoDistribuicao pd);
   
   public void saveListaDistribuicoes();
}
