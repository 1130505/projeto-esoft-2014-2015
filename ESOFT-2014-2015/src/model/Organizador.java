/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Nuno Silva
 */
public class Organizador
{
    private final String m_strNome;
    private Utilizador m_utilizador;

    public Organizador(String strId, Utilizador u )
    {
       m_strNome = u.getNome();
       this.setUtilizador(u);
    }

    private void setUtilizador(Utilizador u)
    {
        m_utilizador = u;
    }
    
    public boolean valida()
    {
        System.out.println("Organizador:valida: " + this.toString());
        return true;
    }
}
