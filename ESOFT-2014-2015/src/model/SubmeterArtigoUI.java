/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *
 * @author Paulo Maio <pam@isep.ipp.pt>
 */
public class SubmeterArtigoUI 
{
    private Empresa m_empresa;
    private SubmeterArtigoController m_controllerSA;

    public SubmeterArtigoUI( Empresa empresa )
    {
        m_empresa = empresa;
        m_controllerSA = new SubmeterArtigoController(m_empresa);
    }

    
}
